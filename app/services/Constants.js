angular.module('jxpro').

constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
}).

constant('PAYMENT_METHODS', {
    'Cash': {
        display:'现金',
        imgUrl: '/image/rmb.png'
    },
    'Wechat': {
        display:'微信',
        imgUrl: '/image/wechat.svg'
    },
    'AliPay': {
        display: '支付宝',
        imgUrl: '/image/alipay.svg'
    },
    'Union': {
        display: '银联',
        imgUrl: '/image/union.svg'
    },
    'Delivery': {
        display: '送货单',
        imgUrl: '/image/delivery.svg'
    }
}).

constant("FILTERS", {
    'page': '分页',
    'all': '所有',
    'unpaid': '未收款',
    'paid': '已收款',
    'pending': '未完成',
    'repaired': '已完成',
    'notAudited': '未查账',
    'opened': '未销单',
    'passedAudit': '已查账',
    'archived': '已销单'
});
