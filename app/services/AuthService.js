angular.module('jxpro').factory('jxpro.AuthService', ['$http', function AuthServiceFactory($http, Session){
    var apiUrl = "http://192.168.0.18:3000/api/"; // PROD
    // var apiUrl = "http://localhost:3000/api/";
    var contentType = 'application/json';

    var authService = {};

    var currentStaff = undefined;

    authService.login = function(credentials){
        var req = {
            method: 'POST',
            url: apiUrl + 'login',
            headers: {
                'Content-Type': contentType
            },
            data: credentials
        };

        return $http(req).then(function(response){
            currentStaff = response.data.staff;
            return response.data.staff;
        });
    };

    authService.isAuthenticated = function(){
        return !!currentStaff;
    };

    authService.getToken = function(){
        if (authService.isAuthenticated()){
            return currentStaff.token;
        }
        return null;
    };

    authService.logout = function (currentStaff) {
        if (authService.isAuthenticated()){
            var req = {
                method: 'POST',
                url: apiUrl + 'logout',
                headers: {
                    'Content-Type': contentType,
                    'Authorization': 'Bearer ' + authService.getToken()
                },
                data: {
                    code: currentStaff.code
                }
            };

            currentStaff = null;
            return $http(req);

        }
        return null;
    };

    return authService;
}]);

