angular.module('jxpro').factory('jxpro.ApiService', ['$http', 'jxpro.AuthService', function ApiServiceFactory($http, AuthService){
    var apiUrl = "http://192.168.0.18:3000/api/"; // PROD
    // var apiUrl = "http://localhost:3000/api/";
    var contentType = 'application/json';

    var apiService = {};

    apiService.updateDeliveryBill = function(oldDeliveryBill, newDeliveryBill, orderId){
        var req = {
            method: 'PUT',
            url: apiUrl + 'repair/deliveryBill/' + orderId,
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                "oldDeliveryBill": oldDeliveryBill,
                "newDeliveryBill": newDeliveryBill
            }
        };

        return $http(req);
    };

    apiService.updateRequisitionBill = function(oldRequisitionBill, newRequisitionBill, orderId){
        var req = {
            method: 'PUT',
            url: apiUrl + 'repair/requisitionBill/' + orderId,
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                "oldRequisitionBill": oldRequisitionBill,
                "newRequisitionBill": newRequisitionBill
            }
        };

        return $http(req);
    };

    apiService.uploadImage = function(fd){
        var req = {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
                // 'Authorization': 'Bearer ' + AuthService.getToken()
            }
        };

        return $http.post(apiUrl + 'repair/upload/', fd, req);
    };

    apiService.getRepairOrderList = function(){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/all/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        };

        return $http(req);
    };

    apiService.changePassword = function (oldPassword, newPassword) {
        var req = {
            method: 'POST',
            url: apiUrl + 'staff/changePassword/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                oldPassword: oldPassword,
                newPassword: newPassword
            }
        };

        return $http(req);
    };

    apiService.getRepairOrderListByPage = function(page){
        var req = {
            method: 'GET',
            url: apiUrl + 'repair/page/' + page,
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        };

        return $http(req);
    };

    apiService.getRepairOrderListByDate = function(dateStart, dateEnd){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/list/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data:{
                dateStart: dateStart,
                dateEnd: dateEnd
            }
        };

        return $http(req);
    };

    apiService.getPaidRepairOrderListByDate = function(dateStart, dateEnd){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/audit/list/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data:{
                dateStart: dateStart,
                dateEnd: dateEnd
            }
        };

        return $http(req);
    };

    apiService.getPaidRepairOrderListByMonth = function(monthCutoff){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/audit/month/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data:{
                monthCutoff: monthCutoff
            }
        };

        return $http(req);
    };

    apiService.getRepairOrderCount = function(){
        var req = {
            method: 'GET',
            url: apiUrl + 'repair/stats/count/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        };

        return $http(req);
    };

    apiService.placeRepairOrder = function (repairOrder) {
        var req = {
            method: 'POST',
            url: apiUrl + 'repair',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: repairOrder
        };

        return $http(req);
    };

    apiService.getRepairOrderById = function(orderId){
        var req = {
            method: 'GET',
            url: apiUrl + 'repair/' + orderId,
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        };

        return $http(req);
    };

    apiService.payRepairOrder = function (payment) {
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/pay',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: payment
        };

        return $http(req);
    };

    apiService.updateRepairOrder = function(repairOrder){
        var req ={
            method: 'PUT',
            url: apiUrl + 'repair/' + repairOrder._id,
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: repairOrder
        };

        return $http(req);
    };

    apiService.archiveRepairOrder = function (repairOrder) {
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/archive/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                id: repairOrder._id
            }
        };

        return $http(req);
    };

    apiService.completeRepairOrder = function (repairOrder) {
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/complete/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                id: repairOrder._id
            }
        };

        return $http(req);
    };

    apiService.assignRepairOrderToMe = function (repairOrder) {
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/assign/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                id: repairOrder._id
            }
        };

        return $http(req);
    };

    apiService.markRepairOrderAudited = function(repairOrder){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/audit/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                id: repairOrder._id
            }
        };

        return $http(req);
    };

    apiService.attachDeliveryBill = function(repairOrder, bill){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/attach/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                id: repairOrder._id,
                deliveryBill: bill
            }
        };

        return $http(req);
    };

    apiService.attachRequisitionBill = function(repairOrder, bill){
        var req = {
            method: 'POST',
            url: apiUrl + 'repair/attach/',
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + AuthService.getToken()
            },
            data: {
                id: repairOrder._id,
                requisitionBill: bill
            }
        };

        return $http(req);
    };

    return apiService;
}]);