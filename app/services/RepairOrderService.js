angular.module('jxpro').factory('jxpro.RepairOrderService', ['jxpro.ApiService', 'jxpro.ToastService', '$q', function RepairOrderServiceFactory(ApiService, ToastService, $q){

    var repairOrderService = {};
    var auditMode = false;
    var currentFilter;

    repairOrderService.getFilter = function(){
        return currentFilter || 'all';
    };

    repairOrderService.setFilter = function(filter){
        currentFilter = filter;
    };

    repairOrderService.setAuditMode = function(flag){
        auditMode = flag;
    };

    repairOrderService.isAuditMode = function(){
        return auditMode;
    };

    repairOrderService.reset = function(){
        currentFilter = undefined;
    };

    repairOrderService.filterList = function(repairOrderList, filter){
        return $q(function(resolve){
            var filteredList = [];
            for (var i = 0; i < repairOrderList.length; i++){

                var repairOrder = repairOrderList[i];

                if (filter === 'all'){
                    filteredList.push(repairOrder);
                } else if (filter === 'paid' && repairOrder.paid && !repairOrder.archived){
                    filteredList.push(repairOrder);
                } else if(filter === 'unpaid' && !repairOrder.paid){
                    filteredList.push(repairOrder);
                } else if(filter === 'pending' && repairOrder.repairStatus == 0){
                    filteredList.push(repairOrder);
                } else if(filter === 'repaired' && repairOrder.repairStatus == 1){
                    filteredList.push(repairOrder);
                } else if(filter === 'passedAudit' && repairOrder.passedAudit){
                    filteredList.push(repairOrder);
                }else if(filter === 'notAudited' && !repairOrder.passedAudit){
                    filteredList.push(repairOrder);
                }else if (filter === 'archived' && repairOrder.archived){
                    filteredList.push(repairOrder);
                }else if (filter === 'opened' && !repairOrder.archived){
                    filteredList.push(repairOrder);
                }else{

                }
            }

            resolve(filteredList);
        });
    };

    repairOrderService.searchFromList = function(repairOrderList, searchStr){
        return $q(function (resolve) {
            var resultList = [];
            for (var i = 0; i < repairOrderList.length; i++){
                var repairOrder = repairOrderList[i];

                if (searchStr == '' || searchStr.length == 0){
                    resultList.push(repairOrder);
                }
                else if(repairOrder.client.includes(searchStr) || repairOrder.description.includes(searchStr) || (repairOrder.phone && repairOrder.phone.includes(searchStr)) || repairOrder.servedBy.name.includes(searchStr) || (repairOrder.technician && repairOrder.technician.name.includes(searchStr)) || hasBill(repairOrder, searchStr)){
                    resultList.push(repairOrder);
                }
            }

            resolve(resultList);
        });
    };

    function hasBill(repairOrder, searchStr){
        var flag = false;
        if (repairOrder.deliveryBill && repairOrder.deliveryBill.code.includes(searchStr)) flag = true;

        if (repairOrder.requisitionBill.length > 0){
            for (var i = 0; i < repairOrder.requisitionBill.length; i++){
                if (!repairOrder.requisitionBill[i].code) {
                    flag = false;
                    console.warn("Fault data detected, order id: ", repairOrder._id);
                    continue;
                }

                if (repairOrder.requisitionBill[i].code.includes(searchStr)) flag = true;
            }
        }


        return flag;
    }

    return repairOrderService;
}]);