angular.module('jxpro').factory('jxpro.DateService', [function DateServiceFactory(){

    var dateService = {};

    var startDate, endDate;

    dateService.getEndDate = function(){
        var today = new Date();
        today.setHours(23, 59, 59);
        return endDate || today;
    };

    dateService.getStartDate = function(){
        if (!startDate){
            startDate = new Date();
            startDate.setHours(0,0,0,0);
            startDate.setDate(dateService.getEndDate().getDate() - 30);
        }
        return startDate;
    };

    dateService.setEndDate = function(ed){
        endDate = ed;
    };

    dateService.setStartDate = function(sd){
        startDate = sd;
    };

    dateService.reset = function(){
        startDate = endDate = undefined;
    };

    return dateService;
}]);
