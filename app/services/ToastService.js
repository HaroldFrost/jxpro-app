angular.module('jxpro').factory('jxpro.ToastService', ['$mdToast', function ToastServiceFactory($mdToast) {

    var toastService = {};

    var last = {
        bottom: true,
        top: false,
        left: false,
        right: true
    };

    var toastPosition = angular.extend({},last);

    var getToastPosition = function() {
        sanitizePosition();

        return Object.keys(toastPosition)
            .filter(function(pos) { return toastPosition[pos]; })
            .join(' ');
    };

    function sanitizePosition() {
        var current = toastPosition;

        if ( current.bottom && last.top ) current.top = false;
        if ( current.top && last.bottom ) current.bottom = false;
        if ( current.right && last.left ) current.left = false;
        if ( current.left && last.right ) current.right = false;

        last = angular.extend({},current);
    }

    toastService.toastErrorMessage = function(msg){
        var toastPosition = getToastPosition();

        $mdToast.show(
            $mdToast.simple().textContent(msg).position(toastPosition).highlightClass('md-accent').hideDelay(3000)
        );
    };

    toastService.toastSuccessMessage = function(msg){
        var toastPosition = getToastPosition();

        $mdToast.show(
            $mdToast.simple().textContent(msg).position(toastPosition).highlightClass('md-primary').hideDelay(3000)
        );
    };

    toastService.toastWarningMessage = function(msg){
        var toastPosition = getToastPosition();

        $mdToast.show(
            $mdToast.simple().textContent(msg).position(toastPosition).highlightClass('md-warn').hideDelay(3000)
        );
    };

    return toastService;
}]);