angular.module('jxpro', [
    'ngMaterial',
    'ngRoute',
    'ngMessages',
    'moment-picker',
    'ngSanitize',
    'ngCsv'
]).

config(function($routeProvider, $locationProvider, $compileProvider, momentPickerProvider){

    momentPickerProvider.options({
        locate: 'zh-cn',
        startView: 'month'
    });

    $compileProvider.preAssignBindingsEnabled(true);

    $locationProvider.hashPrefix('!');

    $routeProvider.when('/login', {
        templateUrl: 'modules/login/login.html',
        controller: 'LoginController'
    });

    $routeProvider.when('/repair/list', {
        templateUrl: 'modules/repairlist/repairlist.html',
        controller: 'RepairListController'
    });

    $routeProvider.when('/repair/order/:orderId', {
        templateUrl: 'modules/repairdetail/repairdetail.html',
        controller: 'RepairOrderController'
    });

    $routeProvider.when('/repair/print/:typeId/:orderId', {
        templateUrl: 'modules/print/print.html',
        controller: 'PrintController'
    });
    
    $routeProvider.when('/repair/activate', {
        templateUrl: 'modules/activate/activate.html',
        controller: 'ActivateController'
    });

    $routeProvider.when('/repair/audit/', {
        templateUrl: 'modules/audit/auditMode.html',
        controller: 'AuditModeController'
    });

    $routeProvider.when('/staff/changePassword', {
        templateUrl: 'modules/changePassword/changePassword.html',
        controller: 'ChangePasswordController'
    });

    $routeProvider.otherwise({
        redirectTo: '/'
    });
}).


controller('ApplicationController', ['$scope', '$timeout', '$mdSidenav', '$location', 'jxpro.AuthService', 'jxpro.ToastService', '$rootScope', 'AUTH_EVENTS', function($scope, $timeout, $mdSidenav, $location, AuthService, ToastService, $rootScope, AUTH_EVENTS){

    $scope.currentStaff = null;

    $scope.backToUrl = undefined;
    $scope.printMode = false;

    $scope.setCurrentStaff = function(staff){
        $scope.currentStaff = staff;
    };

    $scope.logoutStaff = function(){
        $scope.currentStaff = null;
    };

    $scope.goTo = function (url) {
        $location.path(url);
    };

    $scope.$location = $location;
    $scope.$watch('$location.url()', function(newVal, oldVal){
        if(newVal.includes("/repair/print")){
            $scope.printMode = true;
        }
        else{
            $scope.printMode = false;
        }

        if(newVal.includes("/repair/order")){
            $scope.backToUrl = oldVal;
        }else{
            $scope.backToUrl = undefined;
        }
    });

    $scope.goBack = function(){
        if ($scope.backToUrl){
            $location.path($scope.backToUrl);
        }
    };

    $scope.credentials = {
        code: '',
        password: ''
    };

    $scope.logout = function () {
        AuthService.logout($scope.currentStaff).then(function(response){
            ToastService.toastSuccessMessage('已登出！');
            $scope.close();
            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            $scope.setCurrentStaff(null);
        }, function () {

        });
    };

    $scope.login = function(){
        AuthService.login($scope.credentials).then(function(staff){
            ToastService.toastSuccessMessage('登录成功！');

            $scope.setCurrentStaff(staff);
            $location.path("/repair/list");

        }, function(response){
            var toastMsg = "";
            if (response.status == -1){
                toastMsg = "服务器没有响应。"
            }else if (response.status == 401){
                toastMsg = "工号或密码输入错误！";
            }else{
                toastMsg = "登录失败！出现未知错误！"
            }
            ToastService.toastWarningMessage(toastMsg);
        });
    };

    $scope.close = function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav('right').close()
            .then(function () {
                // $log.debug("close RIGHT is done");
            });
    };

    /********************
     * Sidenav controlls
     ********************/
    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.toggleRight = buildDelayedToggler('right');
    $scope.isOpenRight = function(){
        return $mdSidenav('right').isOpen();
    };

    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
        var timer;

        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function() {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
        return debounce(function() {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    // $log.debug("toggle " + navID + " is done");
                });
        }, 200);
    }

    function buildToggler(navID) {
        return function() {
            // Component lookup should always be available since we are not using ` ng-if`
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    // $log.debug("toggle " + navID + " is done");
                });
        }
    }
}]).

controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $location) {
    $scope.close = function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav('left').close()
            .then(function () {
                // $log.debug("close LEFT is done");
            });

    };

    $scope.goTo = function (url) {
        $location.path('/repair' + url);
    }
}).

run(['$rootScope', '$location', 'jxpro.AuthService', '$mdToast', 'AUTH_EVENTS', function($rootScope, $location, AuthService, $mdToast, AUTH_EVENTS){
    $rootScope.$on("$routeChangeStart", function(event, next, current){
        if (!AuthService.isAuthenticated()){
            if (next.templateUrl === "modules/login/login.html"){

            }else{
                $mdToast.show(
                    $mdToast.simple().textContent("请先登录吧！").position('bottom right').hideDelay(3000)
                );
                $location.path("/login");
            }
        }
    });

    $rootScope.$on(AUTH_EVENTS.logoutSuccess, function(event, next, current){
        $location.path("/login");
    });
}]);
