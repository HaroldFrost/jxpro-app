angular.module('jxpro').

controller('AuditModeController', ['$scope','jxpro.ApiService','$mdDialog', '$location', 'AUTH_EVENTS', 'jxpro.ToastService', 'PAYMENT_METHODS', 'FILTERS', 'jxpro.RepairOrderService', function($scope, ApiService, $mdDialog, $location, AUTH_EVENTS, ToastService, PAYMENT_METHODS, FILTERS, RepairOrderService){

    $scope.repairList = {};
    $scope.filteredList = [];
    $scope.loading = false;

    $scope.PAYMENT_METHODS = PAYMENT_METHODS;
    $scope.FILTERS = FILTERS;

    $scope.updateList = function(){
        $scope.loading = true;
        ApiService.getPaidRepairOrderListByDate($scope.dateStart, $scope.dateEnd).then(function(response){
            $scope.repairList = response.data;
            // $scope.filteredList = filterList($scope.filter.currentFilter, $scope.repairList);
            $scope.toggleFilter('all');
            $scope.loading = false;
        }, function(response){
            ToastService.toastErrorMessage('加载列表失败！ 原因： '+ response.data);
            $scope.loading = false;
        });
    };

    var init = function () {
        $scope.settings = {
            filter: {
                currentFilter: 'all',
                activated: false
            },
            searchMode: {
                searchStr: '',
                activated: false
            }
        };

        $scope.dateEnd = new Date();
        $scope.dateStart = new Date();
        $scope.dateStart.setDate($scope.dateStart.getDate()-3);

        $scope.updateList();
    };

    init();

    $scope.search = function(){
        $scope.filteredList = [];
        $scope.settings.filter.currentFilter = 'all';

        $scope.loading = true;
        RepairOrderService.searchFromList($scope.repairList, $scope.settings.searchMode.searchStr).then(function (resultList) {
            $scope.filteredList = resultList;
            $scope.loading = false;
        });
    };

    $scope.toggleFilter = function (filter) {
        $scope.filteredList = [];
        $scope.settings.filter.currentFilter = filter;
        $scope.settings.searchStr = '';
        $scope.loading = true;

        RepairOrderService.filterList($scope.repairList, $scope.settings.filter.currentFilter).then(function(filteredList){
            $scope.filteredList = filteredList;
            $scope.loading = false;
        });
    };

    $scope.goToOrderDetail = function (index) {
        $location.path('/repair/order/' + $scope.filteredList[index]._id);
    };

    $scope.showAuditConfirm = function (ev, index) {
        var repairOrder = $scope.filteredList[index];

        var confirm = $mdDialog.confirm()
            .title('查账')
            .textContent('订单' + repairOrder._id + ' 已经查好账了吗?')
            .ariaLabel('audit confirm')
            .targetEvent(ev)
            .ok('是的')
            .cancel('不是');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.markRepairOrderAudited(repairOrder).then(function (response) {
                $scope.updateList();
                ToastService.toastSuccessMessage('成功标记已查账！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                    $scope.loading = false;
                }
            });
        }, function() {

        });
    };

    $scope.showArchiveConfirm = function(ev, index) {
        var repairOrder = $scope.filteredList[index];

        var confirm = $mdDialog.confirm()
            .title('确认操作')
            .textContent('是否对订单号： ' + repairOrder._id + ' 进行销单？')
            .ariaLabel('archive confirm')
            .targetEvent(ev)
            .ok('确认')
            .cancel('取消');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.archiveRepairOrder(repairOrder).then(function(){
                $scope.updateList();
                ToastService.toastSuccessMessage('成功销单！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                    $scope.loading = false;
                }
            });
        }, function() {

        });
    };

}]);