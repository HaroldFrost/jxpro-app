angular.module('jxpro').

controller('PrintController', ['$scope', '$routeParams', 'jxpro.ApiService', function($scope, $routeParams, ApiService){

    $scope.repairOrder = {};
    $scope.orderId = $routeParams.orderId;
    $scope.typeId = $routeParams.typeId;

    $scope.formattedCreateDate = undefined;
    $scope.formattedPaymentDate = undefined;

    $scope.retrieveOrder = function(orderId){
        ApiService.getRepairOrderById(orderId).then(function(response){
            $scope.repairOrder = response.data;
            $scope.formattedCreateDate = moment($scope.repairOrder.date).tz('Asia/Shanghai').locale('zh-cn').format("LLL");
            $scope.formattedPaymentDate = moment($scope.repairOrder.paymentDate).tz('Asia/Shanghai').locale('zh-cn').format("LLL");
        }, function(response){
            console.log(response);
        });
    };

    $scope.retrieveOrder($scope.orderId);

}]);
