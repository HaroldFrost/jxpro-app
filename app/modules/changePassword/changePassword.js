angular.module('jxpro').

controller('ChangePasswordController', ['$scope', '$rootScope', 'AUTH_EVENTS', 'jxpro.ToastService', 'jxpro.ApiService', function($scope, $rootScope, AUTH_EVENTS, ToastService, ApiService){

    $scope.credentials = {
        oldPassword: '',
        newPassword: '',
        newPasswordRetype: ''
    };

    $scope.changePassword = function(){
        console.log($scope.credentials.oldPassword);
        if ($scope.credentials.newPassword === $scope.credentials.newPasswordRetype){
            ApiService.changePassword($scope.credentials.oldPassword, $scope.credentials.newPassword).then(function(){
                ToastService.toastSuccessMessage("保存成功！");
                $scope.logout();
            }, function(reason){
                ToastService.toastErrorMessage("修改失败！原因： " + reason.data);
            });

        }else{
            ToastService.toastWarningMessage("输入的新密码不一致!");
        }

    };
}]);
