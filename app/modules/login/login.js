angular.module('jxpro').

controller('LoginController', ['$scope', 'jxpro.AuthService', '$rootScope', 'AUTH_EVENTS', '$mdToast', '$location', 'jxpro.ToastService', 'jxpro.ApiService', function($scope, AuthService, $rootScope, AUTH_EVENTS, $mdToast, $location, ToastService, $mdSidenav, ApiService){

    $scope.img = null;

    $scope.credentials = {
        code: '',
        password: ''
    };

    $scope.logout = function () {
        AuthService.logout($scope.currentStaff).then(function(response){
            ToastService.toastSuccessMessage('已登出！');
            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            $scope.close();
            $scope.setCurrentStaff(null);
        }, function (response) {
            ToastService.toastErrorMessage('登出失败！ 原因: ' + response.data);
        });
    };


    $scope.login = function(){
        AuthService.login($scope.credentials).then(function(staff){
            ToastService.toastSuccessMessage('登录成功！');

            $scope.setCurrentStaff(staff);
            $location.path("/repair/list");

        }, function(response){
            var toastMsg = "";
            if (response.status == -1){
                toastMsg = "服务器没有响应。"
            }else if (response.status == 401){
                toastMsg = "工号或密码输入错误！";
            }else{
                toastMsg = "登录失败！出现未知错误！"
            }
            ToastService.toastWarningMessage(toastMsg);
        });
    };

    $scope.paste = function (event, ApiService) {
        var clipData = event.clipboardData;
        var img = null;

        angular.forEach(clipData.items, function (item, key) {
            if (clipData.items[key]['type'].match(/image.*/)) {
                // if it is a image
                img = clipData.items[key].getAsFile();
                console.log('A image sized ' + img.size + ' is being uploaded.');
                var fd = new FormData();
                fd.append('file', img);
                console.log(img);

                ApiService.uploadImage(img).then(function(response){
                    console.log(response);
                }, function(response){
                    console.log(response);
                });
            }
        });
    };

    $scope.close = function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav('right').close()
            .then(function () {
                // $log.debug("close RIGHT is done");
            });
    };
}]);
