/**
 * Created by harold on 11/1/17.
 */
angular.module('jxpro').

controller('RepairListController',['$scope','jxpro.ApiService','$mdDialog', '$location', 'AUTH_EVENTS', 'jxpro.ToastService', 'PAYMENT_METHODS', 'FILTERS', 'jxpro.RepairOrderService', 'jxpro.DateService', function($scope, ApiService, $mdDialog, $location, AUTH_EVENTS, ToastService, PAYMENT_METHODS, FILTERS, RepairOrderService, DateService){

    $scope.repairList = {};
    $scope.filteredList = [];
    $scope.listCsv = [];
    $scope.FILTERS = FILTERS;
    $scope.PAYMENT_METHODS = PAYMENT_METHODS;
    $scope.filename = '';
    $scope.csvHeaders = ['单号', '尾号', '客户', '电话', '内容', '报价', '收款', '成本', '送货单', '调拨单', '开单日期', '收款日期', '开单', '收银', '技师', '收款状态', '维修状态', '查账状态', '销单状态'];

    function updateListCsv(repairOrderList) {
        updateCsvFileName();

        $scope.listCsv = [];

        var i, j, repairOrder, temp, tempList;
        tempList = angular.copy(repairOrderList);
        for (i = 0; i < tempList.length; i++){
            temp = tempList[i];
            repairOrder = {};
            repairOrder.id = temp._id;
            repairOrder.serial = temp._id.toString().substr(temp._id.length - 4, temp._id.length - 1);
            repairOrder.client = temp.client;
            repairOrder.phone = temp.phone ? temp.phone : '无电话';
            
            repairOrder.description = temp.description.toString();
            repairOrder.description = repairOrder.description.replace(/[\n\r]/g, '.');

            repairOrder.quote = temp.quote;
            repairOrder.receipt = temp.receipt;
            repairOrder.cost = temp.cost ? temp.cost : 0;
            repairOrder.deliveryBill = temp.deliveryBill ? temp.deliveryBill.code : '无送货单';

            if (temp.requisitionBill.length <= 0){
                repairOrder.requisitionBill = '无调拨单';
            }else{
                for (j = 0; j < temp.requisitionBill.length; j++){
                    if (j == 0){
                        repairOrder.requisitionBill = temp.requisitionBill[j].code;
                    }else{
                        repairOrder.requisitionBill += ('、 ' + temp.requisitionBill[j].code);
                    }
                }
            }

            repairOrder.date = moment(temp.date).tz('Asia/Shanghai').locale('zh-cn').format("LLL");
            repairOrder.paymentDate = temp.paid ? moment(temp.paymentDate).tz('Asia/Shanghai').locale('zh-cn').format("LLL") : '未收款';
            repairOrder.servedBy = temp.servedBy.name;
            repairOrder.cashier = temp.cashier ? temp.cashier.name : '无';
            repairOrder.technician = temp.technician? temp.technician.name: '未分配';
            repairOrder.paid = temp.paid ? '已付款': '未付款';
            repairOrder.repairStatus = temp.repairStatus == 1 ? '已完成': '未完成';
            repairOrder.passedAudit = temp.passedAudit ? '已查账': '未查账';
            repairOrder.archived = temp.archived ? '已销单' : '未销单';

            $scope.listCsv.push(repairOrder);
        }
    }

    function updateCsvFileName() {
        $scope.filename = '';

        $scope.filename += ($scope.dateStart.getMonth() + 1);
        $scope.filename += '月';
        $scope.filename += $scope.dateStart.getDate();
        $scope.filename += '-';
        $scope.filename += ($scope.dateEnd.getMonth() + 1);
        $scope.filename += '月';
        $scope.filename += $scope.dateEnd.getDate();
        $scope.filename += '.csv';

        return $scope.filename;
    }

    function updateDateService(){
        DateService.setStartDate($scope.dateStart);
        DateService.setEndDate($scope.dateEnd);
    }

    // 审计模式开关
    $scope.auditMode = RepairOrderService.isAuditMode();

    $scope.toggleAuditMode = function(flag){
        $scope.auditMode = flag;
    };

    $scope.updateList = function(){
        $scope.loading = true;

        updateDateService();

        // 如果是审计模式，则获取付款的订单
        if ($scope.auditMode){
            ApiService.getPaidRepairOrderListByDate($scope.dateStart, $scope.dateEnd).then(function(response){
                $scope.repairList = response.data;
                // $scope.filteredList = filterList($scope.filter.currentFilter, $scope.repairList);
                $scope.toggleFilter(RepairOrderService.getFilter());
                $scope.loading = false;
            }, function(response){
                ToastService.toastErrorMessage('加载列表失败！ 原因： '+ response.data);
                $scope.loading = false;
            });
        }
        // 若不是，则获取全部
        else{
            ApiService.getRepairOrderListByDate($scope.dateStart, $scope.dateEnd).then(function(response){
                $scope.repairList = response.data;
                $scope.toggleFilter(RepairOrderService.getFilter());
                $scope.loading = false;
            }, function(response){
                ToastService.toastErrorMessage('加载列表失败！ 原因： '+ response.data);
                $scope.loading = false;
            });
        }
    };

    $scope.$watch('auditMode', function(newVal){
        RepairOrderService.setAuditMode(newVal);
        init();
    });

    $scope.init = function () {
        init();
    };

    var init = function () {
        $scope.settings = {
            filter: {
                currentFilter: RepairOrderService.getFilter(),
                activated: false
            },
            searchMode: {
                searchStr: '',
                activated: false
            }
        };

        $scope.dateEnd = DateService.getEndDate();
        $scope.dateStart = DateService.getStartDate();
        $scope.updateList();
    };

    $scope.hardInit = function(){
        DateService.reset();
        RepairOrderService.reset();
        init();
    };

    $scope.search = function(){
        $scope.filteredList = [];
        $scope.settings.filter.currentFilter = 'all';

        $scope.loading = true;
        RepairOrderService.searchFromList($scope.repairList, $scope.settings.searchMode.searchStr).then(function (resultList) {
            $scope.filteredList = resultList;
            updateListCsv(resultList);
            $scope.loading = false;
        });
    };

    $scope.toggleFilter = function (filter) {
        $scope.filteredList = [];
        $scope.settings.filter.currentFilter = filter;
        RepairOrderService.setFilter(filter);
        $scope.settings.searchStr = '';
        $scope.loading = true;

        RepairOrderService.filterList($scope.repairList, $scope.settings.filter.currentFilter).then(function(filteredList){
            $scope.filteredList = filteredList;
            updateListCsv(filteredList);
            $scope.loading = false;
        });
    };

    $scope.goToOrderDetail = function (index) {
        $location.path('/repair/order/' + $scope.filteredList[index]._id);
    };

    /**
     * @param ev
     */
    $scope.showPrompt = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('What would you name your dog?')
            .textContent('Bowser is a common name.')
            .placeholder('Dog name')
            .ariaLabel('Dog name')
            .initialValue('Buddy')
            .targetEvent(ev)
            .ok('Okay!')
            .cancel('I\'m a cat person');

        $mdDialog.show(confirm).then(function(result) {
            $scope.status = 'You decided to name your dog ' + result + '.';
        }, function() {
            $scope.status = 'You didn\'t name your dog.';
        });
    };

    $scope.showPlaceOrderDialog = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/modules/repairlist/createOrderTemplate.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false
        }).then(function(){
            init();
        }, function () {
            console.log('取消');
        });
    };

    function DialogController($scope, $mdDialog) {
        $scope.repairOrder = {
            client: "",
            phone: "",
            quote: 0.00,
            description: "",
            cost: 0.00
        };

        $scope.placeRepairOrder = function () {
            ApiService.placeRepairOrder($scope.repairOrder).then(function(response){
                ToastService.toastSuccessMessage('成功落单！');
                $mdDialog.hide();
            }, function () {
                ToastService.toastErrorMessage('加载列表失败！ 原因： '+ response.data);
            });
        };

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }

    $scope.showAuditConfirm = function (ev, index) {
        var repairOrder = $scope.filteredList[index];

        var confirm = $mdDialog.confirm()
            .title('查账')
            .textContent('订单' + repairOrder._id + ' 已经查好账了吗?')
            .ariaLabel('audit confirm')
            .targetEvent(ev)
            .ok('是的')
            .cancel('不是');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.markRepairOrderAudited(repairOrder).then(function (response) {
                $scope.updateList();
                ToastService.toastSuccessMessage('成功标记已查账！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                    $scope.loading = false;
                }
            });
        }, function() {

        });
    };

    $scope.showArchiveConfirm = function(ev, index) {
        var repairOrder = $scope.filteredList[index];

        var confirm = $mdDialog.confirm()
            .title('确认操作')
            .textContent('是否对订单号： ' + repairOrder._id + ' 进行销单？')
            .ariaLabel('archive confirm')
            .targetEvent(ev)
            .ok('确认')
            .cancel('取消');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.archiveRepairOrder(repairOrder).then(function(){
                $scope.updateList();
                ToastService.toastSuccessMessage('成功销单！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                    $scope.loading = false;
                }
            });
        }, function() {

        });
    };
}]);