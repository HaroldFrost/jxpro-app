angular.module('jxpro').

controller('RepairOrderController',['$scope','jxpro.ApiService', '$routeParams', '$mdDialog', '$location', 'jxpro.ToastService', 'PAYMENT_METHODS', function($scope, ApiService, $routeParams, $mdDialog, $location, ToastService, PAYMENT_METHODS){

    $scope.repairOrder = {};
    $scope.orderId = $routeParams.orderId;
    $scope.displayCost = false;
    $scope.loading = false;
    $scope.PAYMENT_METHODS = PAYMENT_METHODS;

    $scope.formattedCreateDate = undefined;
    $scope.formattedPaymentDate = undefined;

    $scope.paymentMethods = [
        {value: 'Cash', display:'现金'},
        {value: 'Wechat', display:'微信'},
        {value: 'AliPay', display:'支付宝'},
        {value: 'Union', display:'银联'},
        {value: 'Delivery', display:'送货单'}
    ];

    $scope.toggleCostDisplay = function () {
        $scope.displayCost = !$scope.displayCost;
    };

    $scope.printReceipt = function(){
        $location.path('repair/print/0/' + $scope.repairOrder._id);
    };

    $scope.printInvoice = function(){
        $location.path('repair/print/1/' + $scope.repairOrder._id);
    };

    $scope.retrieveOrder = function(orderId){
        $scope.loading = true;
        ApiService.getRepairOrderById(orderId).then(function(response){
            $scope.repairOrder = response.data;
            $scope.formattedCreateDate = moment($scope.repairOrder.date).tz('Asia/Shanghai').locale('zh-cn').format("LLL");
            $scope.formattedPaymentDate = moment($scope.repairOrder.paymentDate).tz('Asia/Shanghai').locale('zh-cn').format("LLL");
            $scope.loading = false;
        }, function(response){
            ToastService.toastErrorMessage('加载失败！原因： ' + response.data.message);
        });
    };

    $scope.retrieveOrder($scope.orderId);

    /**********************
     ***** MD-DIALOGS *****
     **********************/

    $scope.showEditDeliveryBillPrompt = function(ev){
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('将送货单' + $scope.repairOrder.deliveryBill.code + '改为？')
            .textContent('')
            .placeholder('送货单号')
            .ariaLabel('Delivery Bill')
            .initialValue('')
            .targetEvent(ev)
            .ok('添加')
            .cancel('取消');

        $mdDialog.show(confirm).then(function(billCode) {
            var deliveryBill = {
                code: billCode
            };

            $scope.loading = true;
            ApiService.updateDeliveryBill($scope.repairOrder.deliveryBill, deliveryBill, $scope.repairOrder._id).then(function(response) {
                ToastService.toastSuccessMessage(response.data.message);
                $scope.retrieveOrder($scope.orderId);
                $scope.loading = false;
            }, function(response){
                ToastService.toastErrorMessage(response.data);
                $scope.loading = false;
            });

        }, function(result) {
            console.log(result);
        });
    };

    $scope.showEditRequisitionBillPrompt = function(ev, requisitionBill){
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('将调拨单' + requisitionBill.code + '改为？')
            .textContent('')
            .placeholder('送货单号')
            .ariaLabel('Delivery Bill')
            .initialValue('')
            .targetEvent(ev)
            .ok('添加')
            .cancel('取消');

        $mdDialog.show(confirm).then(function(billCode) {
            var newRequisitionBill = {
                code: billCode
            };

            $scope.loading = true;
            ApiService.updateRequisitionBill(requisitionBill, newRequisitionBill, $scope.repairOrder._id).then(function(response) {
                ToastService.toastSuccessMessage(response.data.message);
                $scope.retrieveOrder($scope.orderId);
                $scope.loading = false;
            }, function(response){
                ToastService.toastErrorMessage(response.data);
                $scope.loading = false;
            });

        }, function(result) {
            console.log(result);
        });
    };

    $scope.showDeliveryBillPrompt = function(ev){
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('请输入送货单号')
            .textContent('')
            .placeholder('送货单号')
            .ariaLabel('Delivery Bill')
            .initialValue('')
            .targetEvent(ev)
            .ok('添加')
            .cancel('取消');

        $mdDialog.show(confirm).then(function(billCode) {
            var deliveryBill = {
                code: billCode
            };

            $scope.loading = true;
            ApiService.attachDeliveryBill($scope.repairOrder, deliveryBill).then(function(response){
                $scope.retrieveOrder($scope.orderId);
                ToastService.toastSuccessMessage('成功添加送货单！');
                $scope.loading = false;
            }, function (response) {
                ToastService.toastErrorMessage('添加送货单失败！原因： ' + response.data.message);
            });

        }, function(result) {
            console.log(result);
        });
    };

    $scope.showRequisitionBillPrompt = function(ev){
        var confirm = $mdDialog.prompt()
            .title('请输入调拨单号')
            .textContent('')
            .placeholder('调拨单')
            .ariaLabel('Requisition Bill')
            .initialValue('')
            .targetEvent(ev)
            .ok('添加')
            .cancel('取消');

        $mdDialog.show(confirm).then(function(billCode) {
            if (billCode === ''){
                $mdDialog.cancel();
            }

            var requisitionBill = {
                code: billCode
            };

            $scope.loading = true;
            ApiService.attachRequisitionBill($scope.repairOrder, requisitionBill).then(function(response){
                $scope.retrieveOrder($scope.orderId);
                ToastService.toastSuccessMessage('成功添加调拨单！');
                $scope.loading = false;
            }, function (response) {
                ToastService.toastErrorMessage('添加调拨单失败！原因： ' + response.data);
            });

        }, function(result) {
            console.log(result);
        });
    };

    $scope.showAuditConfirm = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('查账')
            .textContent('订单' + $scope.repairOrder._id + ' 已经查好账了吗?')
            .ariaLabel('audit confirm')
            .targetEvent(ev)
            .ok('是的')
            .cancel('不是');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.markRepairOrderAudited($scope.repairOrder).then(function (response) {
                $scope.retrieveOrder($scope.orderId);
                ToastService.toastSuccessMessage('成功标记已查账！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                }
            });
        }, function() {

        });
    };

    $scope.showAssignmentConfirm = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('确认分配')
            .textContent('是否将订单' + $scope.repairOrder._id + ' 分配给自己?')
            .ariaLabel('assignment confirm')
            .targetEvent(ev)
            .ok('确认')
            .cancel('取消');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.assignRepairOrderToMe($scope.repairOrder).then(function (response) {
                $scope.retrieveOrder($scope.orderId);
                ToastService.toastSuccessMessage('成功保存分配！');
                $scope.loading = false;
            }, function (response) {
                ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
            });
        }, function() {

        });
    };

    /**
     * Complete order
     */
    $scope.showCompleteConfirm = function(ev){
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('确认操作')
            .textContent('订单号： ' + $scope.repairOrder._id + ' 维修已完成?')
            .ariaLabel('complete confirm')
            .targetEvent(ev)
            .ok('确认')
            .cancel('取消');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.completeRepairOrder($scope.repairOrder).then(function(response){
                $scope.retrieveOrder($scope.orderId);
                ToastService.toastSuccessMessage('成功将订单标记为已完成！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('你没有权限进行此操作。' + response.data);
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                }
            });
        }, function() {

        });
    };

    /**
     * TODO: to be used for quick archive
     * @param ev
     */
    $scope.showArchiveConfirm = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('确认操作')
            .textContent('是否对订单号： ' + $scope.repairOrder._id + ' 进行销单？')
            .ariaLabel('archive confirm')
            .targetEvent(ev)
            .ok('确认')
            .cancel('取消');

        $mdDialog.show(confirm).then(function() {
            $scope.loading = true;
            ApiService.archiveRepairOrder($scope.repairOrder).then(function(){
                $scope.retrieveOrder($scope.orderId);
                ToastService.toastSuccessMessage('成功销单！');
                $scope.loading = false;
            }, function (response) {
                if (response.status == 403){
                    ToastService.toastSuccessMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }else{
                    ToastService.toastSuccessMessage('保存失败！原因： '+ response.data);
                    $scope.loading = false;
                }
            });
        }, function() {

        });
    };

    $scope.showEditDialog = function(ev) {
        $mdDialog.show({
            controller: EditDialogController,
            templateUrl: '/modules/repairdetail/editDetailTemplate.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                repairOrder: $scope.repairOrder
            }
        }).then(function(){
            $scope.retrieveOrder($scope.orderId);
        }, function(){
            $scope.retrieveOrder($scope.orderId);
        });
    };

    $scope.showPaymentDialog = function(ev) {
        $mdDialog.show({
            controller: PaymentDialogController,
            templateUrl: '/modules/repairdetail/paymentTemplate.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                orderId: $scope.repairOrder._id,
                paymentMethods: $scope.paymentMethods,
                quote: $scope.repairOrder.quote
            }
        }).then(function(){
            $scope.retrieveOrder($scope.orderId);
        }, function () {
        });
    };

    /******************************
     ***** DIALOG Controllers *****
     ******************************/

    function PaymentDialogController($scope, $mdDialog, orderId, paymentMethods, quote) {
        $scope.payment = {
            id: orderId,
            paymentMethod: '',
            receipt: quote
        };

        $scope.paymentMethods = paymentMethods;

        $scope.payRepairOrder = function () {
            $scope.loading = true;
            ApiService.payRepairOrder($scope.payment).then(function(response){
                ToastService.toastSuccessMessage('成功保存收款！');
                $mdDialog.hide(response);
                $scope.loading = false;
            }, function(response){
                if (response.status == 403){
                    ToastService.toastWarningMessage('保存失败！你没有权限进行此操作。');
                    $scope.loading = false;
                }
                ToastService.toastWarningMessage('保存失败！原因： '+ response.data);
            });
        };

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }

    function EditDialogController($scope, $mdDialog, repairOrder) {
        $scope.repairOrder = repairOrder;

        $scope.updateRepairOrder = function () {
            $scope.loading = true;
            ApiService.updateRepairOrder($scope.repairOrder).then(function(response){
                ToastService.toastSuccessMessage('成功保存信息！');
                $mdDialog.hide();
                $scope.loading = false;
            }, function (response) {
                ToastService.toastErrorMessage('保存失败！原因： '+ response.data);
            });
        };

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }
}]);